"use client";
import Head from "next/head";

export default function Page() {
  return (
    <div>
      <main>
        <h1>@Team</h1>

        <p>
          <code>app/@team</code>
        </p>
      </main>
    </div>
  );
}
