"use client";
import Head from "next/head";

export default function Page() {
  return (
    <div>
      <main>
        <h1>@Analytics</h1>

        <p>
          <code>app/@analytics</code>
        </p>
      </main>
    </div>
  );
}
